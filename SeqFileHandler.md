# SeqFileHandler Tests

## ToDo
* Some Ion Torrent single-end samples fails.

## SetUp

```python

>>> from SeqFileHandler import SeqFile

>>> import os.path
>>> import inspect
>>> class_file = inspect.getfile(SeqFile)
>>> root_dir = os.path.dirname(os.path.realpath(class_file))
>>> test_data_dir = ("{}/test/".format(root_dir))

>>> test_filenames = [
...     "DTU2017-1115-PRJ1066-CPH-Sewage-141_R1_001.fq.gz",
...     "DTU2017-1115-PRJ1066-CPH-Sewage-141_R2_001.fq.gz",
...     "DTU2017-1116-PRJ1066-CPH-Sewage-142_R1_001.fq.gz",
...     "DTU2017-1116-PRJ1066-CPH-Sewage-142_R2_001.fq.gz",
...     "ERR1432965_1.fastq.gz",
...     "ERR1432965_2.fastq.gz",
...     "ERR1512990_1.fastq.gz",
...     "ERR1512990_2.fastq.gz",
...     "ERR1512999_1.fastq.gz",
...     "ERR1512999_2.fastq.gz",
...     "SRR5063020.fastq.gz",
...     "SRR5633389_1.fastq.gz",
...     "SRR5633389_2.fastq.gz",
...     "SRR5827429_1.fastq.gz",
...     "SRR5827429_2.fastq.gz"]
>>> test_files = []
>>> for filename in test_filenames:
...     test_files.append("{}/{}".format(test_data_dir, filename))

```

## __init__(self, seqfile, pe_file_reverse=None, phred_format=None)

```python

>>> SeqFile("missing_file")
... #doctest: +ELLIPSIS
Traceback (most recent call last):
FileNotFoundError: Sequence file not found: ...missing_file

>>> seqfile = SeqFile("{}/SRR5063020.fastq.gz".format(test_data_dir),
...                   phred_format="unknown")
>>> seqfile.filename
'SRR5063020'
>>> print(seqfile.filename_reverse)
None
>>> seqfile.gzipped
True
>>> seqfile.seq_format
'single'

>>> seqfile2 = SeqFile("{}/ERR1432965_1.fastq.gz".format(test_data_dir),
...     pe_file_reverse="{}/ERR1432965_2.fastq.gz".format(test_data_dir),
...     phred_format="unknown")
>>> seqfile2.seq_format
'paired'

```

## get_read_filename(seq_path)

```python

>>> test_path = "some/path/to/file.fq.fastq.txt.trim.gz "
>>> SeqFile.get_read_filename(test_path)
'file'

```

## is_gzipped(file_path)

```python

>>> SeqFile.is_gzipped(class_file)
False
>>> SeqFile.is_gzipped("{}/ERR1432965_1.fastq.gz".format(test_data_dir))
True

```

## predict_pair(file_path)

```python

>>> nextseq_name1 = "some/path/Some_name22-B_S53_L002_R1_001.fastq.gz"
>>> nextseq_name2 = "some/path/Some_name22-B_S53_L002_R2_001.fastq.gz"
>>> errname1 = "some/path/ERR1432965_1.fastq.gz"
>>> errname2 = "some/path/ERR1432965_2.fastq.gz"
>>> srr_single_name = "some/path/SRR5063020.fastq.gz"
>>> srrname1 = "/some/path/SRR5633389_1.fastq.gz"
>>> srrname2 = "SRR5633389_2.fastq.gz"
>>> SeqFile.predict_pair(nextseq_name1)
1
>>> SeqFile.predict_pair(nextseq_name2)
2
>>> SeqFile.predict_pair(errname1)
1
>>> SeqFile.predict_pair(errname2)
2
>>> SeqFile.predict_pair(srr_single_name)
0
>>> SeqFile.predict_pair(srrname1)
1
>>> SeqFile.predict_pair(srrname2)
2

```

## parse_files(cls, file_paths, phred=None, headers2count=10, min_match=2, force_neighbour=False, ignore_errors=False)

```python

>>> seqfiles = SeqFile.parse_files(test_files, phred="unknown")
>>> seqfiles[0].filename
'DTU2017-1115-PRJ1066-CPH-Sewage-141_R1_001'
>>> seqfiles[0].path
... #doctest: +ELLIPSIS
'...test/DTU2017-1115-PRJ1066-CPH-Sewage-141_R1_001.fq.gz'
>>> seqfiles[0].filename_reverse
'DTU2017-1115-PRJ1066-CPH-Sewage-141_R2_001'
>>> seqfiles[0].path_reverse
... #doctest: +ELLIPSIS
'...test/DTU2017-1115-PRJ1066-CPH-Sewage-141_R2_001.fq.gz'
>>> seqfiles[0].seq_format
'paired'

>>> seqfiles[1].filename
'DTU2017-1116-PRJ1066-CPH-Sewage-142_R1_001'
>>> seqfiles[1].filename_reverse
'DTU2017-1116-PRJ1066-CPH-Sewage-142_R2_001'
>>> seqfiles[1].seq_format
'paired'

>>> seqfiles[2].filename
'ERR1432965_1'
>>> seqfiles[2].filename_reverse
'ERR1432965_2'

>>> seqfiles[3].filename
'ERR1512990_1'
>>> seqfiles[3].filename_reverse
'ERR1512990_2'

>>> seqfiles[4].filename
'ERR1512999_1'
>>> seqfiles[4].filename_reverse
'ERR1512999_2'

>>> seqfiles[5].filename
'SRR5063020'
>>> seqfiles[5].seq_format
'single'

>>> seqfiles[6].filename
'SRR5633389_1'
>>> seqfiles[6].filename_reverse
'SRR5633389_2'

>>> seqfiles[7].filename
'SRR5827429_1'
>>> seqfiles[7].filename_reverse
'SRR5827429_2'

```

## Tests ToDo

* detect_pair_no(header1, header2)
* group_fastqs(file_paths)
* concat_fastqs(file_paths, out_path=".", verbose=False)
* parse_files(cls, file_paths, phred=None, headers2count=10, min_match=2, force_neighbour=True, ignore_errors=False)
* load_seq_files(file_paths)
* checksum(fname, method="sha1")
